# IOC功能之依赖注入

> 主讲: 赵云

DI: Dependency Injection, 依赖注入, 也称为"装配"

为Srping框架管理的bean的属性注入值

从生命周期角度来说, DI操作在对象的创建之后

-----

## 1. 简单值装配

简单值: 8种基本数据类型及对应的包装类型, String, Class, Resource, 这些Spring都可以做简单值装配

```java
public class SomeClass {
    private int attrA;
    private Double attrB;
    private String attrC;
    private Class attrD;
    // getters & setters
}
```

```xml
<bean id="some" class="demo.SomeClass">
    <property name="attrA" value="123"/>
    <property name="attrB" value="123"/>
    <property name="attrC" value="fdagfdah"/>
    <property name="attrD" value="java.lang.String"/>
</bean>
```

```java
ApplicationContext ac = new ClassPathXmlApplicationContext("app*.xml");
SomeClass some = (SomeClass) ac.getBean("some");
System.out.println(some);
```

-----

## 2. 集合类型的装配

包括数组/List/Set/Map/Properties

```java
public class SomeClass {
    private String[] arr;
    private List<String> list;
    private Set<String> set;
    private Map<String, String> map;
    private Properties prop;
    // getters & setters
}
```

```xml
<bean id="some" class="demo.SomeClass">
    <!-- private String[] arr; -->
    <property name="arr">
        <array>
            <value>aaa</value>
            <value>bbb</value>
            <value>ccc</value>
        </array>
    </property>
    <!-- 配置方式2: 通过value属性装配, 元素用逗号分隔 -->
    <!--
        <property name="arr" value="aaa,bbb,ccc"/>
    -->
    <!-- ======================================= -->
    <!-- private List<String> list; -->
    <property name="list">
        <list>
            <value>123</value>
            <value>456</value>
            <value>789</value>
        </list>
    </property>
    <!-- private Set<String> set; -->
    <property name="set">
        <set>
            <value>aaa123</value>
            <value>bbb123</value>
            <value>ccc123</value>
        </set>
    </property>
    <!-- List与Set, 当只有一个元素时, 可以直接通过value属性装配 -->
    <!--
        <property name="list" value="abc"/>
        <property name="set" value="abc"/>

        这样写没有用, 集合中还是只有一个元素, 而该元素是: "aaa,bbb,ccc"
        <property name="list" value="aaa,bbb,ccc"/>
    -->
    <!-- ======================================= -->
    <!-- private Map<String, String> map; -->
    <property name="map">
        <map>
            <entry key="a" value="123"/>
            <entry key="nb" value="456"/>
            <entry key="c" value="789"/>
        </map>
    </property>
    <!-- ======================================= -->
    <!-- private Properties prop; -->
    <property name="prop">
        <props>
            <prop key="a">a123</prop>
            <prop key="b">bn123</prop>
            <prop key="c">ca123</prop>
        </props>
    </property>
    <!-- 当properties只有一条属性配置时, 可以通过value属性配置 -->
    <!--
        <property name="prop" value="a=123"/>

        这样写没用, properties对象只装配一项属性: a - 123,b=456
        <property name="prop" value="a=123,b=456"/>
    -->

</bean>
```

```java
ApplicationContext ac = new ClassPathXmlApplicationContext("app*.xml");
SomeClass some = (SomeClass) ac.getBean("some");
System.out.println(some);
```

-----

## 3. Resource类型的装配

`org.springframework.core.io.Resource`

该类型在Spring框架中使用很频繁

该接口常用实现类: 

- 从文件系统中加载资源

    `org.springframework.core.io.FileSystemResource`

    `org.springframework.core.io.FileUrlResource`

- 从类加载路径加载资源

    `org.springframework.core.io.ClassPathResource`

```java
public class SomeClass {
    private Resource res1;
    private Resource res2;
    // getters & setters
}
```

```xml
<bean id="some" class="demo.SomeClass">

    <!-- 此处创建的对象真实类型是FileUrlResource -->
    <property name="res1" value="file:e:/demo.sql"/>

    <!-- ClassPathResource -->
    <property name="res2" value="classpath:test.txt"/>

    <!-- 由于该接口有两个不同的实现类, 分别表示从文件系统加载资源和从类加载路径加载资源 -->
    <!-- Spring框架在创建Resource的具体对象时, 如何判断该创建哪个实现类的对象 -->
    <!-- 取决于标签的value属性如何标识 -->
    <!-- 如果是FileSystemResource, value中的资源路径前需要加"file:"标识 -->
    <!-- 如果是ClassPathResource, value中的资源路径前需要加"claspath:"标识 -->

    <!-- "classpath:" 与 "classspath*:" 都可以加载事整个类加载路径下(包括jar包中的内容)的资源文件 -->

    <!-- classpath: -->
    <!-- 该标识只会返回第一个匹配到的资源, 优先查找工程中的资源文件, 再查找jar包 -->
    <!-- 表示告诉web容器去classpath(/WEB-INF/classes和/WEB-INF/lib)中加载指定名称的资源 -->
    <!-- 如果有同名资源, 则只会加载一个 -->

    <!-- classpath*: -->
    <!-- 告诉web容器去classpath中加载指定名称的资源 -->
    <!-- 如果有同名资源, 全都会加载 -->

    <!-- 注意: 非web工程用"classpath*:"会获取不到资源 -->

</bean>
```

```java
ApplicationContext ac = new ClassPathXmlApplicationContext("app*.xml");
SomeClass some = (SomeClass) ac.getBean("some");
System.out.println(some);
System.out.println(some.getRes1().getFile());
System.out.println(some.getRes2().getFile().getAbsolutePath());
```

-----

## 4. 其它Bean的引用装配

```java
public interface SomeDao {}
public class SomeDaoImpl implements SomeDao {}
```

```java
public interface SomeService {
    void soSome();
}

public class SomeServiceImpl implements SomeService {
    private SomeDao someDao;
    // 当成员变量被private修饰, 它就不再表示属性
    // 真正代表属性的是get方法/set方法
    // 要让框架进行属性装配, 需要提供相应的set方法
    public void setSomeDao(SomeDao someDao) {
        this.someDao = someDao;
    }

    @Override
    public void soSome() {
        System.out.println(this.someDao);
    }
}
```

```xml
<bean id="someDao" class="demo.dao.impl.SomeDaoImpl"/>
<bean id="str" class="java.lang.String"/>
<bean id="someService" class="demo.service.impl.SomeServiceImpl">
    <!-- ref: references, 引用, 参考 -->
    <!-- 标签的ref属性配置的是对另一个bean的引用 -->
    <!-- 它的值是引用bean标签的id属性值 -->
    <property name="someDao" ref="someDao"/>
    <!-- 注意: ref指向的bean的类型, 必须与property表示的属性的类型一致 -->
</bean>
```

```java
ApplicationContext ac = new ClassPathXmlApplicationContext("app*.xml");
SomeService service = (SomeService) ac.getBean("someService");
service.soSome();
```

-----

## 5. 属性编辑器

```java
public class Address {
    private String province; // 省
    private String city; // 市
    // getters & setters
}
```

```java
public class User {
    private String name;
    private Address address;
    // getters & setters
}
```

- 引用装配方式

    ```xml
    <bean id="address" class="demo.Address">
        <property name="province" value="JiangSu"/>
        <property name="city" value="NanJing"/>
    </bean>

    <bean id="user" class="demo.User">
        <property name="name" value="ZhangSan"/>
        <property name="address" ref="address"/>
    </bean>
    ```

- 属性编辑器装配方式

    ```java
    // 要使用属性编辑器, 需要自定义相关的编辑器
    // 该编辑器需要extends PropertyEditorSupport
    // 示例中是要将字符串转换成目标属性
    // 就需要重写setAsText(String text)
    public class AddressPropertyEditor extends PropertyEditorSupport {
        // 在spring配置中希望将一个字符串注入到一个引用类型属性上
        // 从方法名上可以看出, 以字符串进行设置
        @Override
        public void setAsText(String text) throws IllegalArgumentException {
            // 在该方法中实现: 将一个字符串变成Address对象
            // 按照希望的格式对字符串进行拆分并封装对象的处理
            // 参数text就是配置文件中所写的字符串
            String[] arr = text.split("-");
            Address obj = new Address();
            obj.setProvince(arr[0]);
            obj.setCity(arr[1]);
            // 要用封装好的对象进行属性装配, 调用方法
            setValue(obj);
        }
    }
    ```

    ```xml
    <!-- 属性编辑器 -->
    <!-- 希望直接将"省份-市区"这样格式的字符串装配到属性上 -->
    <!-- 编写好属性编辑器类之后, 要在配置中告诉spring怎么用它 -->
    <!-- 该bean标签不需要指明id值 -->
    <!-- 该bean配置的是spring框架的配置信息类: 用户自定义编辑器信息 -->
    <bean class="org.springframework.beans.factory.config.CustomEditorConfigurer">
        <!-- 该类中有属性: customEditors -->
        <!-- 将用户自定义的编辑器配置到该属性中 -->
        <!-- 该属性装配的是一个Map -->
        <property name="customEditors">
            <map>
                <!-- 装配Map时, key是编辑器要转换得到的目标类型 -->
                <!-- value: 就是用户自定义的编辑器的完整类名 -->
                <entry key="demo.Address" value="demo.AddressPropertyEditor"/>
                <!-- 当遇到目标类型是key指明的类型时, 就用value指明的编辑器进行转换处理 -->
            </map>
        </property>
    </bean>

    <!-- 有了属性编辑器bean之后, 就可以直接配置User的bean -->
    <bean id="user" class="demo.User">
        <property name="name" value="ZhangSan"/>
        <property name="address" value="江苏-南京"/>
    </bean>
    ```

```java
ApplicationContext ac = new ClassPathXmlApplicationContext("app*.xml");
User u = (User) ac.getBean("user");
System.out.println(u);
```

-----

## 6. 配置继承

```java
public class Parent {
    private String name;
    private int age;
    // getters & setters
}
```

```java
public class Child extends Parent {
    private String password;
    // getters & setters
}
```

```xml
<bean id="parent" class="demo.Parent">
    <property name="name" value="angelina"/>
    <property name="age" value="23"/>
</bean>

<!-- 子类继承了父类的2个属性, 子类对象一共有3个属性可配置 -->
<bean id="child" class="demo.Child">
    <property name="name" value="ZhangSan"/>
    <property name="age" value="25"/>
    <property name="password" value="123456"/>
</bean>

<!-- 该child对象的name属性值及age属性值与父类对象的属性值一致 -->
<!-- 通过bean标签的parent属性, 让该配置项继承另一个bean的配置项 -->
<!-- 属性值就写目标bean标签的id值 -->
<!-- 这里所说的继承, 是配置项与配置项之间的继承, 而非类的继承 -->
<!-- 即使是没有继承关系的两个java类, 也可以在配置上发生继承 -->
<bean id="child2" class="demo.Child" parent="parent">
    <property name="password" value="123456"/>
</bean>
```

```java
ApplicationContext ac = new ClassPathXmlApplicationContext("app*.xml");

Parent p = (Parent) ac.getBean("parent");
System.out.println(p);

Child c1 = (Child) ac.getBean("child");
System.out.println(c1);

Child c2 = (Child) ac.getBean("child2");
System.out.println(c2);
```

-----

## 7. 配置抽象类

```java
public abstract class Parent {
    private String name;
    private int age;
    // getters & setters
}
```

```java
public class Child extends Parent {
    private String password;
    // getters & setters
}
```

```xml
<!-- demo.Parent是一个抽象类, 无法创建对象 -->
<!-- 如果该标签配置又需要存在 -->
<!-- 就需要通过abstract="true"属性告诉spring框架, 这是一个抽象类, 不需要创建对象 -->
<bean id="parent" class="demo.Parent" abstract="true">
    <property name="name" value="angelina"/>
    <property name="age" value="23"/>
</bean>

<!-- 被标记为abstract的bean标签, 虽然不会创建对象 -->
<!-- 但不影响其它bean标签继承配置项 -->
<bean id="child2" class="demo.Child" parent="parent">
    <property name="password" value="123456"/>
</bean>
```

```java
ApplicationContext ac = new ClassPathXmlApplicationContext("app*.xml");
// 由于配置中的对应的bean标签配置的是abstract类
// 不会创建对象, 所以这里获取不到对象
// 如果强行获取会报错
// Parent p = (Parent) ac.getBean("parent");
// System.out.println(p);
Child c2 = (Child) ac.getBean("child2");
System.out.println(c2);
```

-----

## 8. 访问外部配置文件装配属性

```java
public class User {
    private String name;
    private int age;
    // getters & setters
}

```

外部配置文件: user.properties

```properties
user.name=Angelina
user.age=23
```

spring配置

```xml
<!-- 在容器中配置一个bean: PropertyPlaceholderConfigurer, 该bean是用来读取配置文件的 -->
<!-- 该bean由框架内部自动使用, 所以不需要配置id属性 -->
<bean class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">

    <!-- location属性, 可以加载一个资源, 属性类型是Resource -->
    <property name="location" value="classpath:user.properties"/>

    <!-- locations属性可以加载多个资源, 属性类型是Resource[] -->
    <!--<property name="locations">
        <array>
            <value>classpath:user.properties</value>
        </array>
    </property>-->

</bean>

<bean id="user" class="demo.User">
    <!-- 当通过PropertyPlaceholderConfigurer加载配置文件之后 -->
    <!-- 可以在配置中通过类似EL表达式的语法读取配置中的内容 -->
    <property name="name" value="${user.name}"/>
    <property name="age" value="${user.age}"/>
</bean>
```

```java
ApplicationContext ac = new ClassPathXmlApplicationContext("app*.xml");
User u = (User) ac.getBean("user");
System.out.println(u);
```

-----

## 9. 属性的自动装配

```java
public class SomeClass {}
```

```java
public class OtherClass {
    private SomeClass some1;
    private SomeClass some2;

    public OtherClass() {}

    public OtherClass(SomeClass some1, SomeClass some2) {
        this.some1 = some1;
        this.some2 = some2;
    }
    // getters & setters
}
```

```xml
<bean id="some1" class="demo.SomeClass"/>
<bean id="some2" class="demo.SomeClass"/>
<!--<bean id="some2" class="java.lang.String"/>-->

<!-- 要让spring自动注入属性, 需要给bean标签设置属性: autowire -->
<bean id="other" class="demo.OtherClass" autowire="constructor"/>
<!-- autowire属性取值 -->

<!-- 1. no: 不进行自动注入, 必须通过property标签进行注入 -->

<!-- 2. byName: 根据要注入的属性名实现自动装配 -->
<!--    spring会在ac容器中寻找id值和属性名一致的bean -->
<!--    将找到的bean注入到属性中, 如果没找到bean则不进行注入 -->
<!--    如果找到的bean的类型, 与属性的类型不一致, 报错 -->

<!-- 3. byType: 根据属性的类型进行自动装配 -->
<!--    spring会在ac容器中寻找类型与属性类型一致的bean -->
<!--    将找到的bean注入到属性中, 如果找不到则不进行注入 -->
<!--    如果找到的bean不唯一 -->
<!--    比如配置了多个类型一样的bean -->
<!--    或同一接口的多个实现 -->
<!--    报错 -->

<!-- 4. constructor: 根据构造方法参数, 使用byType方式进行注入 -->
<!--    使用这种方式, 会根据构造方法进行注入 -->
<!--    根据构造方法的参数类型, 以byType的形式在ac容器中匹配bean -->
<!--    使用该方式注入时, spring不是通过set方法进行属性注入的 -->
<!--    也就是说, 采用该方式时, 属性可以没有set方法 -->

<!-- 5. default: 行为和no一样 -->
```

```java
ApplicationContext ac = new ClassPathXmlApplicationContext("app*.xml");
OtherClass other = (OtherClass) ac.getBean("other");
System.out.println(other);
```