# SPRING框架IOC与AOP

-----

# 常用GIT命令

| 命令 | 说明 |
| ---: | :--- |
| `git clone <URL>` | 将远程版本库克隆到本地(URL: 远程版本库地址) |
| `git pull` | 从远程版本库拉取更新 |
| `git add .` | 暂存本地版本库中所有修改过的文件 |
| `git add -A` | 暂存本地版本库中所有修改过的文件 |
| `git add --all` | 暂存本地版本库中所有修改过的文件 |
| `git add <file>` | 暂存本地版本库中修改过的指定文件 |
| `git commit -m 'message'` | 提交暂存的更新 |
| `git push` | 将提交的更新上传到远程版本库 |
| `git status` | 查看本地版本库状态 |
| `git fetch` | 本地版本库commit之后push之前使用, 查看本地版本与远程版本差异 |

`.gitignore`文件: 该文件中指定的文件或目录, 不会被GIT进行版本管理, 可用通配符

| 通配符 | 说明 | 例 |
| ---: | :--- | --- |
| `?` | 匹配一个字符 | `/a?b.txt` |
| `*` | 匹配多个字符 | `/*.txt` |
| `**` | 匹配多层路径 | `/**/*.iml` |
| `!` | 表示不排除 | `!/src/abc.txt` |

-----

![](./image/Git常用命令速查表.jpg)

-----

GIT完整命令

![](./image/git完整命令.png)